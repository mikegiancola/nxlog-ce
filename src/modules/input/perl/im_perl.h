/*
 * This file is part of the nxlog log collector tool.
 * Website: http://nxlog.org
 * Author: Ivan Baidakou <the.dmol@gmail.com>
 * License:
 * Copyright (C) 2015 by Botond Botyanszki
 * This library is free software; you can redistribute it and/or modify
 * it under the same terms as Perl itself, either Perl version 5.8.5 or,
 * at your option, any later version of Perl 5 you may have available.
 */

#ifndef __NX_IM_PERL_H
#define __NX_IM_PERL_H

#define USE_ITHREADS

#include "../../../common/types.h"
#include "../../../common/module.h"
#include "../../extension/perl/libnxperl.h"

#include <EXTERN.h>
#include <perl.h>

#endif	/* __NX_IM_PERL_H */
