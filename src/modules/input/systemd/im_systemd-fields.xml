<fields>
  <module>im_systemd</module>
  <field>
    <name>raw_event</name>
    <type>string</type>
    <persist>FALSE</persist>
    <description>
      <en>
	A list of event fields in key-value pairs.
      </en>
    </description>
  </field>

  <field>
    <name>Message</name>
    <type>string</type>
    <persist>FALSE</persist>
    <description>
      <en>
      	A human-readable message string for the current entry.
      	This is supposed to be the primary text shown to the user.
      	This is usually not translated (but might be in some cases),
      	and not supposed to be parsed for metadata.
      </en>
    </description>
  </field>

  <field>
    <name>MessageID</name>
    <type>string</type>
    <persist>FALSE</persist>
    <description>
      <en>
        A 128-bit message identifier for recognizing certain message
        types, if this is desirable. This should contain a 128-bit identifier
        formatted as a lower-case hexadecimal string, without any
        separating dashes or suchlike. This is recommended to be
        a UUID-compatible ID, but this is not enforced, and formatted
        differently.
      </en>
    </description>
  </field>

  <field>
    <name>Severity</name>
    <type>string</type>
    <persist>FALSE</persist>
    <description>
      <en>
        A priority value between 0 ("emerg") and 7 ("debug")
        formatted as a string. This field is compatible with
        syslog's priority concept.
      </en>
    </description>
  </field>

  <field>
    <name>SeverityValue</name>
    <type>integer</type>
    <persist>FALSE</persist>
    <description>
      <en>
        A priority value between 0 ("emerg") and 7 ("debug")
        formatted as a decimal string. This field is compatible with
        syslog's priority concept.
      </en>
    </description>
  </field>

  <field>
    <name>CodeFile</name>
    <type>string</type>
    <persist>FALSE</persist>
    <description>
      <en>
        Code location to generate this message, if known.
        Contains the source filename.
      </en>
      </description>
    </field>

    <field>
      <name>CodeLine</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Code location to generate this message, if known.
          Contains the line number.
        </en>
      </description>
    </field>

    <field>
      <name>CodeFunc</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Code location to generate this message, if known.
          Contains the function name.
        </en>
      </description>
    </field>

    <field>
      <name>Errno</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Low-level Unix error number which caused the entry, if any.
          Contains the numeric value of 'errno' formatted as a decimal string.
        </en>
      </description>
    </field>

    <field>
      <name>Facility</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Syslog compatibility fields containing the facility.
        </en>
      </description>
    </field>

    <field>
      <name>SourceName</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Syslog compatibility field containing the identifier
          string (i.e. "tag").
        </en>
      </description>
    </field>

    <field>
      <name>ProcessID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Syslog compatibility field containing the client PID.
        </en>
      </description>
    </field>

    <field>
      <name>User</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          User ID of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>Group</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Group ID of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>ProcessName</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Name of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>ProcessExecutable</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Executable path of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>ProcessCmdLine</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Command line of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>Capabilities</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Effective capabilities of the process the journal entry originates
          from.
        </en>
      </description>
    </field>

    <field>
      <name>AuditSession</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Session of the process the journal entry originates from,
          as maintained by the kernel audit subsystem.
        </en>
      </description>
    </field>

    <field>
      <name>AuditUID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Login UID of the process the journal entry originates from,
          as maintained by the kernel audit subsystem.
        </en>
      </description>
    </field>


    <field>
      <name>SystemdCGroup</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Control group path in the systemd hierarchy of the process the
          journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>SystemdSession</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Systemd session ID (if any) of the process the journal entry
          originates from.
        </en>
      </description>
    </field>

    <field>
      <name>SystemdUnit</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Systemd unit name (if any) of the process the journal entry
          originates from.
        </en>
      </description>
    </field>

    <field>
      <name>SystemdUserUnit</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Systemd user session unit name (if any) of the process the
          journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>SystemdOwnerUID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Owner UID of the systemd session (if any)
          of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>SystemdSlice</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Systemd slice unit of the process the journal entry originates from.
        </en>
      </description>
    </field>

    <field>
      <name>AuditUID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Login UID of the process the journal entry originates from,
          as maintained by the kernel audit subsystem.
        </en>
      </description>
    </field>

    <field>
      <name>SelinuxContext</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          SELinux security context (label) of the process the journal entry
          originates from.
        </en>
      </description>
    </field>

    <field>
      <name>EventTime</name>
      <type>datetime</type>
      <persist>FALSE</persist>
      <description>
        <en>
          The earliest trusted timestamp of the message,
          if any is known that is different from the reception
          time of the journal.
        </en>
      </description>
    </field>

    <field>
      <name>BootID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Kernel boot ID for the boot the message was
          generated in, formatted as a 128-bit hexadecimal string.
        </en>
      </description>
    </field>

    <field>
      <name>MachineID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Machine ID of the originating host.
        </en>
      </description>
    </field>

    <field>
      <name>SysInvID</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Invocation ID for the runtime cycle of the
          unit the message was generated in, as available
          to processes of the unit in $INVOCATION_ID.
        </en>
      </description>
    </field>

    <field>
      <name>Hostname</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          The name of the originating host.
        </en>
      </description>
    </field>

    <field>
      <name>Transport</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Transport of the entry to the journal service. Available values are:
          audit, driver, syslog, journal, stdout, kernel.
        </en>
      </description>
    </field>


    <field>
      <name>KernelDevice</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Device name of the kernel.
          If the entry is associated to a block device, the field contains the
          major and minor of the device node, separated by ":" and prefixed by
          "b". Similar for character devices but prefixed by "c". For network
          devices, this is the interface index prefixed by "n". For all other
          devices, this is the subsystem name prefixed by "+", followed by ":",
          followed by the kernel device name.
        </en>
      </description>
    </field>

    <field>
      <name>KernelSubsystem</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Subsystem name of the kernel.
        </en>
      </description>
    </field>

    <field>
      <name>DevName</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Device name of the kernel as it shows up in the device tree under the
          '/sys' directory.
        </en>
      </description>
    </field>

    <field>
      <name>DevNode</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Node path of the device under the '/dev' directory.
        </en>
      </description>
    </field>

    <field>
      <name>DevLink</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Additional symlink names pointing to the device node under the '/dev'
          directory.
        </en>
      </description>
    </field>

    <field>
      <name>CoredumpUnit</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Annotation to the message in case it contains coredumps from system
          and session units.
        </en>
      </description>
    </field>

    <field>
      <name>CoredumpUserUnit</name>
      <type>string</type>
      <persist>FALSE</persist>
      <description>
        <en>
          Annotation to the message in case it contains coredumps from system
          and session units.
        </en>
      </description>
    </field>

    <field>
      <name>ObjProcessID</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'ProcessID', except that the
          process identified by PID is described, instead of the process which
          logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjUser</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'User', except that the
          process identified by PID is described, instead of the process which
          logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjGroup</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'Group', except that the
          process identified by PID is described, instead of the process which
          logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjUser</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same name as the 'User', except that the
          process identified by PID is described, instead of the process which
          logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjProcessName</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'ProcessName', except that
          the process identified by PID is described, instead of the process
          which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjProcessExecutable</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'ProcessExecutable', except
          that the process identified by PID is described, instead of the
          process which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjProcessCmdLine</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'ProcessCmdLine', except
          that the process identified by PID is described, instead of the
          process which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjAuditSession</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'AuditSession', except that
          the process identified by PID is described, instead of the process
          which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjAuditUID</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'AuditUID', except that
          the process identified by PID is described, instead of the process
          which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjSystemdCGroup</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'SystemdCGroup', except
          that the process identified by PID is described, instead of the
          process which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjSystemdSession</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'SystemdSession', except
          that the process identified by PID is described, instead of the
          process which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjSystemdUnit</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'SystemdUnit', except that
          the process identified by PID is described, instead of the process
          which logged the message.
        </en>
      </description>
    </field>

    <field>
      <name>ObjSystemdOwnerUID</name>
      <type>integer</type>
      <persist>FALSE</persist>
      <description>
        <en>
          This field contains the same value as the 'SystemdOwnerUID', except
          that the process identified by PID is described, instead of the
          process which logged the message.
        </en>
      </description>
    </field>

</fields>
