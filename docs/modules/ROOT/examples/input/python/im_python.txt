REMOVE: tmp/output

#WIN
#SETENVVAR: PYTHONHOME=/mingw64
#END

STARTDAEMON: modules/input/python/im_python.conf
SLEEP: 1
STOPDAEMON: modules/input/python/im_python.conf
COMPAREFILE: tmp/output modules/input/python/input.txt
REMOVE: tmp/output

STARTDAEMON: modules/input/python/2_im_python.conf
SLEEP: 1
STOPDAEMON: modules/input/python/2_im_python.conf
COMPAREFILE: tmp/output modules/input/python/2_output.txt
REMOVE: tmp/output
