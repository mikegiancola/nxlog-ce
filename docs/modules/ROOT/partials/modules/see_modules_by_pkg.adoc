ifndef::skipModuleByPackage[]

////
// This is not needed for the CE Reference Manual.
See the <<modules_by_package_{module},list of installer packages>> that
provide the _{module}_ module in the Available Modules chapter of the
{productName} User Guide.
////

endif::skipModuleByPackage[]
