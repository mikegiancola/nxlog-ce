[id='{module}_config_sslprotocol']
SSLProtocol:: This directive can be used to set the allowed SSL/TLS
  protocol(s). It takes a comma-separated list of values which can be any of
  the following: `SSLv2`, `SSLv3`, `TLSv1`, `TLSv1.1`, `TLSv1.2`, and `TLSv1.3`.
  By default, the `TLSv1.2` and `TLSv1.3` protocols are allowed. Note that the
  OpenSSL library shipped by Linux distributions may not support `SSLv2` and
  `SSLv3`, and these will not work even if enabled with this directive.
